# risc-v-kernel

#### 介绍
用于制作 `openEuler RISC-V`版本的内核镜像，内核源代码来自 openEuler/kernel 仓库的 OLK-5.10 分支。

当使用 obs 构建时，利用OBS的 tar_scm 插件自动完成 kernel 源码的获取；如果想要进行本地构建，需要手动获取 openeuler 的 kernel 源码并手动打包成 kernel.spec 文件中所
指定的 “.tar” 文件。

Eg. For version '5.10', 内核源码包链接如下：

    https://gitee.com/openeuler/kernel/repository/archive/OLK-5.10.zip

在 rpm 包构建完成之后，会得到一个未经压缩的 kernel Image，配合 `openSBI`<https://gitee.com/src-openeuler/opensbi> 即可制作出可通过 QEMU 启动的内核镜像。

#### 软件架构
RISC-V64


#### 安装教程
1.  repo源配置：配置`openEuler RISC-V`提供的repo地址<https://repo.openeuler.org/openEuler-preview/RISC-V/everything/>作为repo源
2.  kernel安装
```
    dnf install risc-v-kernel opensbi
```

#### 使用说明

1.  获取此 repo 构建得到的 rpm 包， 如， kernel-5.10.riscv64.rpm，安装此 rpm 包，或通过如下命令得到 rpm 包中的文件：

```
    rpm2cpio kernel-*.riscv64.rpm | cpio -id
```

2.  如果在上一步骤中是直接安装了 rpm 包，则在 /boot 目录下有 Image 和 config-5 文件，然后请安装
openSBI。安装完成后，在 /boot 目录下会出现 fw_payload_oe_qemuvirt.elf，即为QEMU启动的内核镜像。

    如果在上一步骤中是解压缩得到了内核镜像，那么后续制作基于 QEMU 内核镜像的步骤详情见 <https://gitee.com/src-openeuler/opensbi>

#### 参与贡献

1.  Fork 本仓库
2.  新建 x.y.z 分支
3.  提交代码
4.  新建 Pull Request

### Thanks
